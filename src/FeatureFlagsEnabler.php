<?php

namespace Trinetus\LaravelFeatureFlags;

interface FeatureFlagsEnabler
{
    public function getFieldValueForFeatureFlags(string $fieldName): ?array;
}
