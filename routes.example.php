<?php

/**
 * @codeCoverageIgnore
 */
Route::get(
    'admin/feature_flags/example',
    [
        'uses' => '\Trinetus\LaravelFeatureFlags\ExampleController@seeTwitterField',
        'as' => 'laravel-feature-flag.example'
    ]
);
